#include <math.h>

#if !defined(CartVecH)
#define CartVecH

// Structure representing a cartesian vector, position or otherwise

struct CartVec
{
    // Values ij, xy
    float i = 0, j = 0;


    // Function to normalise the vector
    // post : magnitude of vector will be 1
    void Normalise();
    // Function to get the perpendicular vector to this
    // OUT  : normal vector to this
    CartVec GetPerp() const;

    // Function to get the scalar product with another CartVec
    //  OUT : float, scalar product
    //IN/OUT: other CartVec
    float dotProd(const CartVec& other) const;


    inline CartVec operator +(const CartVec& other) const
    {
        return { i + other.i, j + other.j };
    }
    inline CartVec operator -(const CartVec& other) const
    {
        return { i - other.i, j - other.j };
    }
    inline CartVec operator *(float& other) const
    {
        return { other * i, other * j };
    }
    inline CartVec operator *(int& other) const
    {
        return { i * other, j * other };
    }
};


#endif