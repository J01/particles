#include "CartVec.h"

void CartVec::Normalise()
{
    float mag = sqrtf(i * i + j * j);
    float det = 1 / mag;
    i = i * det;
    j = j * det;
}
CartVec CartVec::GetPerp() const
{
    return { j, -i };
}
float CartVec::dotProd(const CartVec& other) const
{
    float result = 0;
    result += i * other.i;
    result += j * other.j;
    return result;
}
