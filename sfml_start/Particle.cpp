#include "Particle.h"

void Particle::Update(float elapsed)
{
    s = s + (v * elapsed);
}


bool ParticleCollision::CheckCollision(const Particle& p1, const Particle& p2)
{
    float sRadSq = powf(p1.r + p2.r, 2);

    float dxSq, dySq;
    dxSq = powf(p2.s.i - p1.s.i, 2);
    dySq = powf(p2.s.j - p1.s.j, 2);

    return sRadSq >= (dxSq + dySq);
}
void ParticleCollision::Collide(Particle& p1, Particle& p2)
{
    //create line of centres from p1 to p2
    CartVec lineOfC = (p2.s - p1.s);
    lineOfC.Normalise();
    //get perp vector
    CartVec perpC = lineOfC.GetPerp();

    //find components perp and parallel
    float ax, bx, ay, by;
    ax = p1.v.dotProd(lineOfC);
    bx = p2.v.dotProd(lineOfC);

    ay = p1.v.dotProd(perpC);
    by = p2.v.dotProd(perpC);

    //NEL e = Vsep / Vapp
    float e = (p1.e + p2.e / 2);
    float eVapp = e * (ax - bx);

    //CLM start P = end P
    float CLMa = p1.m * ax + p2.m * bx;

    float px, qx;
    //py = ay, qy = by

    px = (CLMa - p2.m * eVapp) / (p1.m + p2.m);
    qx = (CLMa + p1.m * eVapp) / (p1.m + p2.m);

    p1.v = lineOfC * px + perpC * ay;
    p2.v = lineOfC * qx + perpC * by;

}