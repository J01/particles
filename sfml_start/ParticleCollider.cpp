#include "ParticleCollider.h"
#include <ctime>

void ParticleCollider::Initialise()
{
    srand(time(0));

    for (int i = 0; i < ParticleColliderConsts::numberParticles; i++)
    {
        p[i].s = { (float)(60 + (i % 5) * 80), (float)(60 + ((i - (i % 5)) / 5) * 80) };

        int angle = rand() % 360;
        p[i].v = { cosf((float)angle * 3.14f / 180.f), sinf((float)angle * 3.14f / 180.f) };

        int v = rand() % (ParticleColliderConsts::maxV - ParticleColliderConsts::minV) + ParticleColliderConsts::minV;
        p[i].v = p[i].v * v;

        int m = rand() % (ParticleColliderConsts::maxM - ParticleColliderConsts::minM) + ParticleColliderConsts::minM;
        p[i].m = (float)m;

        int r = rand() % (ParticleColliderConsts::maxR - ParticleColliderConsts::minR) + ParticleColliderConsts::minR;
        p[i].r = (float)r;

        float e = (rand() / RAND_MAX);
        p[i].e = e;
    }
}

void ParticleCollider::Update(const float elapsed)
{
    for (int i = 0; i < ParticleColliderConsts::numberParticles; i++)
    {
        p[i].Update(elapsed);
        CheckCollisionsWithBox(p[i]);
    }

    for (int i = 0; i < ParticleColliderConsts::numberParticles; i++)
    {
        for (int j = i + 1; j < ParticleColliderConsts::numberParticles; j++)
        {
            if (ParticleCollision::CheckCollision(p[i], p[j]))
            {
                ParticleCollision::Collide(p[i], p[j]);
            }
        }
    }
}

void ParticleCollider::Draw(sf::RenderWindow& window) const
{
    for (int i = 0; i < ParticleColliderConsts::numberParticles; i++)
    {
        DrawP(p[i], window);
    }
}


void ParticleCollider::DrawP(const Particle& p, sf::RenderWindow& window) const 
{
    sf::CircleShape pShape;
    pShape.setRadius(p.r);
    pShape.setOrigin(p.r, p.r);
    pShape.setPosition(p.s.i, p.s.j);

    //change amount of red based on mass
    //change amount of blue based on e
    sf::Color col = sf::Color();
    col.b = (int)(p.e * 25);
    col.r = (int)(p.m * 50);

    pShape.setFillColor(col);

    window.draw(pShape);
}

void ParticleCollider::CheckCollisionsWithBox(Particle& p)
{
    if (p.s.i - p.r < LeftBound)
    {
        p.s.i = LeftBound + p.r;
        p.v.i = -p.v.i;
    }
    else if (p.s.i + p.r > RightBound)
    {
        p.s.i = RightBound - p.r;
        p.v.i = -p.v.i;
    }
    if (p.s.j - p.r < TopBound)
    {
        p.s.j = TopBound + p.r;
        p.v.j = -p.v.j;
    }
    else if (p.s.j + p.r > BottomBound)
    {
        p.s.j = BottomBound - p.r;
        p.v.j = -p.v.j;
    }

};