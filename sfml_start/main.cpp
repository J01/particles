#include "SFML/Graphics.hpp"
#include "ParticleCollider.h"



int main()
{
	// Create the main window
	sf::RenderWindow window(sf::VideoMode(600, 600), "Collisions in 2D with Newton's Experimental Law");

	ParticleCollider pc;
	pc.Initialise();

    sf::Clock clk;
    clk.restart();
	
    float elapsed;

	// Start the game loop 
	while (window.isOpen())
	{
		// Process events
		sf::Event event;
		while (window.pollEvent(event))
		{
			// Close window: exit
			if (event.type == sf::Event::Closed || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) 
				window.close();
		} 

		// Clear screen
		window.clear();

        elapsed = clk.getElapsedTime().asSeconds();
        clk.restart();

		pc.Update(elapsed);
		pc.Draw(window);

		// Update the window
		window.display();

	}

	return EXIT_SUCCESS;
}
